Zadatak se sastoji od izrade baze podataka i jednostavne aplikacije za obrađivanje podataka.
Za bazu koristiti MSSQL Express 2014 a za izradu aplikacije koristiti ASP.NET MVC 5 i sve njegove komponente.
Zadatak smo osmilili kao malenu aplikaciju za unos djelatnika i njihovih radnih zadataka unutar neke tvrtke, 
te dodjelu radnih zadataka odabranim djelatnicima.
Sukladno tome sastaviti bazu podataka sa dvije tablice i poljima unutar njih:

Tablica Djelatnici:

- ID djelatnika (BigInt)

-          Ime Djelatnika (varchar (100))

-          Prezime Djelatnika (Varchar(250))

Tavblica Radni Zadaci:

-          ID Radnog zadatka (bigint)

-          Naslov radnog zadatka (varchar(250))

-          Opis radnog zadatka (varchar(MAX))

-          ID djelatnika (bigint)

 
Tablice povezati foreign key-om preko polja ID Djelatnika.
Nakon definiranja baze potrebno napraviti malenu aplikaciju u kojoj će biti jedna forma za unos djelatnika i njihovih podataka,
te drugu formu za unos radnih zadataka te dodjelu radnih zadataka određenom djelatniku. 

Na formi unosa i izmjena radnih zadataka omogućiti dodavanje djelatnika radnom zadatku iz nekakvog padajućeg izbornika ili nečeg sličnog.
Želimo da forma radnih zadataka sama nudi već postojeće djelatnike za odabir bez potrebe ručnog unosa.
Također potrebno je omogućiti izmjene i brisanje pojedinih djelatnika i radnih zadataka.